package com.r0adkll.postoffice;

import com.r0adkll.postoffice.widgets.MaterialButtonLayout;

import android.graphics.drawable.StateListDrawable;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ViewFlipper;
import anywheresoftware.b4a.BA;

public class R {
   public static final class attr {
   }

   public static final class drawable {
     public static int edittext_mtrl_alpha=BA.applicationContext.getResources().getIdentifier("edittext_mtrl_alpha", "drawable", BA.packageName);

   }

   public static final class id {
     public static int title= BA.applicationContext.getResources().getIdentifier("title", "id", BA.packageName);
     public static int message= BA.applicationContext.getResources().getIdentifier("message", "id", BA.packageName);
     public static int message_scrollview= BA.applicationContext.getResources().getIdentifier("message_scrollview", "id", BA.packageName);
     public static int content_frame= BA.applicationContext.getResources().getIdentifier("content_frame", "id", BA.packageName);
     public static int style_content= BA.applicationContext.getResources().getIdentifier("style_content", "id", BA.packageName);
     public static int button_container= BA.applicationContext.getResources().getIdentifier("button_container", "id", BA.packageName);
     public static int icon= BA.applicationContext.getResources().getIdentifier("icon", "id", BA.packageName);
     public static int alertTitle= BA.applicationContext.getResources().getIdentifier("alertTitle", "id", BA.packageName);
     public static int titleDivider= BA.applicationContext.getResources().getIdentifier("titleDivider", "id", BA.packageName);
     public static int customPanel= BA.applicationContext.getResources().getIdentifier("customPanel", "id", BA.packageName);
     public static int topPanel= BA.applicationContext.getResources().getIdentifier("topPanel", "id", BA.packageName);
     public static int contentPanel= BA.applicationContext.getResources().getIdentifier("contentPanel", "id", BA.packageName);
     public static int ripple_button= BA.applicationContext.getResources().getIdentifier("ripple_button", "id", BA.packageName);
     
     
   }

   public static final class layout {
     public static int simple_listitem_mtrl_light   = BA.applicationContext.getResources().getIdentifier("simple_listitem_mtrl_light", "layout", BA.packageName);
     public static int simple_listitem_mtrl_dark    = BA.applicationContext.getResources().getIdentifier("simple_listitem_mtrl_dark", "layout", BA.packageName);
     public static int layout_material_light_dialog = BA.applicationContext.getResources().getIdentifier("layout_material_light_dialog", "layout", BA.packageName);
     public static int layout_material_dark_dialog  = BA.applicationContext.getResources().getIdentifier("layout_material_dark_dialog", "layout", BA.packageName);
     public static int layout_holo_dialog           = BA.applicationContext.getResources().getIdentifier("layout_holo_dialog", "layout", BA.packageName);
     public static int material_light_dialog_button = BA.applicationContext.getResources().getIdentifier("material_light_dialog_button", "layout", BA.packageName);
     public static int material_dark_dialog_button  = BA.applicationContext.getResources().getIdentifier("material_dark_dialog_button", "layout", BA.packageName);
   }

   public static final class raw {
     public static int libarm = BA.applicationContext.getResources().getIdentifier("libarm", "raw", BA.packageName);
   }

   public static final class string {
     public static int mediacontroller_play_pause = BA.applicationContext.getResources().getIdentifier("mediacontroller_play_pause", "string", BA.packageName);
     public static int vitamio_videoview_error_title = BA.applicationContext.getResources().getIdentifier("vitamio_videoview_error_title", "string", BA.packageName);
   }

   public static final class style {
     public static int MediaController_SeekBar = BA.applicationContext.getResources().getIdentifier("MediaController_SeekBar", "style", BA.packageName);
   }

   public static class dimen {
	   public static int default_margin = BA.applicationContext.getResources().getIdentifier("default_margin", "dimen", BA.packageName);
	   public static int default_margin_small = BA.applicationContext.getResources().getIdentifier("default_margin_small", "dimen", BA.packageName);
	   public static int material_edittext_spacing  = BA.applicationContext.getResources().getIdentifier("material_edittext_spacing", "dimen", BA.packageName);
	   public static int material_button_height  = BA.applicationContext.getResources().getIdentifier("material_button_height", "dimen", BA.packageName);
	   
   }
   
	public static class color {
		public static int background_material_dark= BA.applicationContext.getResources().getIdentifier("background_material_dark", "color", BA.packageName);
		public static int background_material_light= BA.applicationContext.getResources().getIdentifier("background_material_light", "color", BA.packageName);
		public static int grey_400= BA.applicationContext.getResources().getIdentifier("grey_400", "color", BA.packageName);
		public static int grey_800= BA.applicationContext.getResources().getIdentifier("grey_800", "color", BA.packageName);
		public static int red_200= BA.applicationContext.getResources().getIdentifier("red_200", "color", BA.packageName);
        public static final int rippelColor=0x7f040004;
        public static final int ripple_material_dark=0x7f040010;
        public static final int ripple_material_light=0x7f04000f;

   }
	
	public static class integer {
		public static int ripple_duration= BA.applicationContext.getResources().getIdentifier("ripple_duration", "integer", BA.packageName);
   }
	
	public static class styleable {
		  public static final int[] RippleView = {
	            0x7f01000b, 0x7f01000c, 0x7f01000d, 0x7f01000e,
	            0x7f01000f, 0x7f010010, 0x7f010011, 0x7f010012,
	            0x7f010013, 0x7f010014
	        };
	        public static final int RippleView_rv_alpha = 0;
	        public static final int RippleView_rv_centered = 5;
	        public static final int RippleView_rv_color = 4;
	        public static final int RippleView_rv_framerate = 1;
	        public static final int RippleView_rv_rippleDuration = 2;
	        public static final int RippleView_rv_ripplePadding = 7;
	        public static final int RippleView_rv_type = 6;
	        public static final int RippleView_rv_zoom = 8;
	        public static final int RippleView_rv_zoomDuration = 3;
	        public static final int RippleView_rv_zoomScale = 9;
   }

	
   
}
