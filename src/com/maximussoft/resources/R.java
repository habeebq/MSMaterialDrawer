//package com.maximussoft.resources;
//
//import anywheresoftware.b4a.BA;
//
//public class R {
//	public static final class color {
//		public static int material_drawer_primary_text = BA.applicationContext.getResources().getIdentifier("material_drawer_primary_text", "color", BA.packageName);
//		public static int material_drawer_hint_text = BA.applicationContext.getResources().getIdentifier("material_drawer_hint_text", "color", BA.packageName);
//		public static int material_drawer_selected_text = BA.applicationContext.getResources().getIdentifier("material_drawer_selected_text", "color", BA.packageName);
//		public static int material_drawer_secondary_text = BA.applicationContext.getResources().getIdentifier("material_drawer_secondary_text", "color", BA.packageName);
//		public static int material_drawer_selected = BA.applicationContext.getResources().getIdentifier("material_drawer_selected", "color", BA.packageName);
//	}
//	public static final class dimen {
//		public static int material_drawer_margin = BA.applicationContext.getResources().getIdentifier("material_drawer_margin", "dimen", BA.packageName);
//	}
//	public static final class layout {
//		public static int drawer_slider = BA.applicationContext.getResources().getIdentifier("drawer_slider", "layout", BA.packageName);
//		public static int drawer_item_header = BA.applicationContext.getResources().getIdentifier("drawer_item_header", "layout", BA.packageName);
//		public static int drawer_item_footer = BA.applicationContext.getResources().getIdentifier("drawer_item_footer", "layout", BA.packageName);
//		public static int drawer_item_divider = BA.applicationContext.getResources().getIdentifier("drawer_item_divider", "layout", BA.packageName);
//		public static int drawer_item_primary = BA.applicationContext.getResources().getIdentifier("drawer_item_primary", "layout", BA.packageName);
//		public static int drawer_item_secondary = BA.applicationContext.getResources().getIdentifier("drawer_item_secondary", "layout", BA.packageName);
//		public static int drawer_item_section = BA.applicationContext.getResources().getIdentifier("drawer_item_section", "layout", BA.packageName);
//	}
//	public static final class string {
//		public static int drawer_close = BA.applicationContext.getResources().getIdentifier("drawer_close", "string", BA.packageName);
//	}
//	public static final class id {
//		public static int icon = BA.applicationContext.getResources().getIdentifier("icon", "id", BA.packageName);
//		public static int name = BA.applicationContext.getResources().getIdentifier("name", "id", BA.packageName);
//		public static int badge = BA.applicationContext.getResources().getIdentifier("badge", "id", BA.packageName);
//		public static int divider = BA.applicationContext.getResources().getIdentifier("divider", "id", BA.packageName);
//	}
//}
