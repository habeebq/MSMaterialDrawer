package com.maximussoft.msmaterialdrawer;

import android.graphics.drawable.Drawable;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.objects.collections.List;

import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.iconics.Iconics;
import com.mikepenz.iconics.Iconics.IconicsBuilder;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.typeface.ITypeface;

@BA.ShortName("MSIconicDrawable")
public class IconicDrawable {
	IconicsDrawable mIDrawable;
	
	public Drawable Initialize(BA ba, String icon){
		mIDrawable = new IconicsDrawable(ba.context, icon);
		Drawable dw = mIDrawable; 
		return dw;
	}
	
	public Drawable getDrawable(){
		return (Drawable)mIDrawable;
	}
	
	public List ListDrawables(BA ba, String typeface){
		List retList = new List();
		retList.Initialize();
		ITypeface tp = Iconics.findFont(typeface);
		for (String value : tp.getIcons()){
			retList.Add(value);
		}
		return retList;
	}
	
	public void setAlpha(int alpha){
		mIDrawable.setAlpha(alpha);
	}
	
	public int getAlpha(){
		return mIDrawable.getAlpha();
	}
	
	public void setColor(int color){
		mIDrawable.color(color);
	}
	
	public void setBackgroundColor(int backgroundColor){
		mIDrawable.backgroundColor(backgroundColor);
	}
	
	public void actionBarSize(){
		mIDrawable.actionBarSize();
	}
	
	public void paddingDp(int iconPadding){
		mIDrawable.paddingDp(iconPadding);
	}
	
	public void sizeDp(int size){
		mIDrawable.sizeDp(size);
	}
	
	
}
