package com.maximussoft.msmaterialdrawer;

import java.util.ArrayList;
import java.util.Collections;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader.OnAccountHeaderListener;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader.OnAccountHeaderSelectionViewClickListener;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import anywheresoftware.b4a.AbsObjectWrapper;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BA.DependsOn;

@BA.ShortName("MSAccountHeaderBuilder")
@BA.Events(values = {
		"ProfileChanged(Profile as MSProfile)",
		"ProfileClicked(Profile as MSProfile)"})

@BA.ActivityObject
public class MSAccountHeaderBuilder extends AbsObjectWrapper<AccountHeader> {
	private BA mBA;
	private String mEventName;
	private AccountHeader.Result result = null;

	public void Initialize(BA ba, String eventName){
		mBA = ba;
	    mEventName = eventName;
	   setObject(new AccountHeader().withActivity(ba.activity));
	   
	   getObject().withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
			@Override
		public boolean onProfileChanged(View view, IProfile profile,
				boolean current) {
				MSProfile p = new MSProfile();
				p = (MSProfile) profile;
				mBA.raiseEvent2(this, false, (mEventName + "_ProfileChanged").toLowerCase(mBA.cul),false, new Object[]{p});
			
			return false;
		}
	});
	   
	   getObject().withOnAccountHeaderSelectionViewClickListener(new AccountHeader.OnAccountHeaderSelectionViewClickListener() {
		
		@Override
		public boolean onClick(View view, IProfile profile) {
			MSProfile p = new MSProfile();
			p = (MSProfile) profile;
			mBA.raiseEvent2(this, false, (mEventName + "_ProfileClicked").toLowerCase(mBA.cul),false, new Object[]{p});
			return true;
		}
	});
	}

	    /**
	     * Defines if we should use the compact style for the header.
	     *
	     * @param compactStyle
	     * @return
	     */
	    public MSAccountHeaderBuilder withCompactStyle(boolean compactStyle) {
	        getObject().withCompactStyle(compactStyle);
	        return this;
	    }
	    /**
	     * set the height for the header
	     *
	     * @param heightDp
	     * @return
	     */
	    public MSAccountHeaderBuilder withHeightDp(int heightDp) {
	        getObject().withHeightDp(heightDp);
	        return this;
	    }
	    /**
	     * set the background for the slider as color
	     *
	     * @param textColor
	     * @return
	     */
	    public MSAccountHeaderBuilder withTextColor(int textColor) {
	        getObject().withTextColor(textColor);
	        return this;
	    }
	    /**
	     * set this to false if you want to hide the first line of the selection box in the header (first line would be the name)
	     *
	     * @param selectionFirstLineShown
	     * @return
	     */
	    public MSAccountHeaderBuilder withSelectionFistLineShown(boolean selectionFirstLineShown) {
	        getObject().withSelectionFistLineShown(selectionFirstLineShown);
	        return this;
	    }

	    /**
	     * set this to false if you want to hide the second line of the selection box in the header (second line would be the e-mail)
	     *
	     * @param selectionSecondLineShown
	     * @return
	     */
	    public MSAccountHeaderBuilder withSelectionSecondLineShown(boolean selectionSecondLineShown) {
	        getObject().withSelectionSecondLineShown(selectionSecondLineShown);
	        return this;
	    }
	    /**
	     * set this to define the first line in the selection area if there is no profile
	     * note this will block any values from profiles!
	     *
	     * @param selectionFirstLine
	     * @return
	     */
	    public MSAccountHeaderBuilder withSelectionFirstLine(String selectionFirstLine) {
	    	getObject().withSelectionFirstLine(selectionFirstLine);
	        return this;
	    }

	    /**
	     * set this to define the second line in the selection area if there is no profile
	     * note this will block any values from profiles!
	     *
	     * @param selectionSecondLine
	     * @return
	     */
	    public MSAccountHeaderBuilder withSelectionSecondLine(String selectionSecondLine) {
	    	getObject().withSelectionSecondLine( selectionSecondLine);
	        return this;
	    }
	    /**
	     * Set or disable this if you use a translucent statusbar
	     *
	     * @param translucentStatusBar
	     * @return
	     */
	    public MSAccountHeaderBuilder withTranslucentStatusBar(boolean translucentStatusBar) {
	    	getObject().withTranslucentStatusBar (translucentStatusBar);
	        return this;
	    }
	    /**
	     * set the background for the slider as color
	     *
	     * @param headerBackground
	     * @return
	     */
	    public MSAccountHeaderBuilder withHeaderBackground(Drawable headerBackground) {
	    	getObject().withHeaderBackground(headerBackground);
	        return this;
	    }
	    /**
	     * define the ScaleType for the header background
	     *
	     * @param headerBackgroundScaleType
	     * @return
	     */
	    public MSAccountHeaderBuilder withHeaderBackgroundScaleType(ImageView.ScaleType headerBackgroundScaleType) {
	    	getObject().withHeaderBackgroundScaleType(headerBackgroundScaleType);
	        return this;
	    }
	    /**
	     * define if the profile images in the header are shown or not
	     *
	     * @param profileImagesVisible
	     * @return
	     */
	    public MSAccountHeaderBuilder withProfileImagesVisible(boolean profileImagesVisible) {
	    	getObject().withProfileImagesVisible( profileImagesVisible);
	        return this;
	    }
	    /**
	     * enable or disable the profile images to be clickable
	     *
	     * @param profileImagesClickable
	     * @return
	     */
	    public MSAccountHeaderBuilder withProfileImagesClickable(boolean profileImagesClickable) {
	    	getObject().withProfileImagesClickable(profileImagesClickable);
	        return this;
	    }
	    /**
	     * enable the alternative profile header switching
	     *
	     * @param alternativeProfileHeaderSwitching
	     * @return
	     */
	    public MSAccountHeaderBuilder withAlternativeProfileHeaderSwitching(boolean alternativeProfileHeaderSwitching) {
	    	getObject().withAlternativeProfileHeaderSwitching(alternativeProfileHeaderSwitching);
	        return this;
	    }
	    /**
	     * enable the extended profile icon view with 3 small header images instead of two
	     *
	     * @param threeSmallProfileImages
	     * @return
	     */
	    public MSAccountHeaderBuilder withThreeSmallProfileImages(boolean threeSmallProfileImages) {
	    	getObject().withThreeSmallProfileImages(threeSmallProfileImages);
	        return this;
	    }
	    /**
	     * set a onSelection listener for the selection box
	     *
	     * @param onAccountHeaderSelectionViewClickListener
	     * @return
	     */
	    public MSAccountHeaderBuilder withOnAccountHeaderSelectionViewClickListener(OnAccountHeaderSelectionViewClickListener onAccountHeaderSelectionViewClickListener) {
	    	getObject().withOnAccountHeaderSelectionViewClickListener(onAccountHeaderSelectionViewClickListener);
	        return this;
	    }
	    /**
	     * enable or disable the selection list if there is only a single profile
	     *
	     * @param selectionListEnabledForSingleProfile
	     * @return
	     */
	    public MSAccountHeaderBuilder withSelectionListEnabledForSingleProfile(boolean selectionListEnabledForSingleProfile) {
	    	getObject().withSelectionListEnabledForSingleProfile(selectionListEnabledForSingleProfile);
	        return this;
	    }
	    /**
	     * enable or disable the selection list
	     *
	     * @param selectionListEnabled
	     * @return
	     */
	    public MSAccountHeaderBuilder withSelectionListEnabled(boolean selectionListEnabled) {
	    	getObject().withSelectionListEnabled(selectionListEnabled);
	        return this;
	    }
	    /**
	     * You can pass a custom view for the drawer lib. note this requires the same structure as the drawer.xml
	     *
	     * @param accountHeader
	     * @return
	     */
	    public MSAccountHeaderBuilder withAccountHeader(View accountHeader) {
	    	getObject().withAccountHeader(accountHeader);
	        return this;
	    }
	    /**
	     * set the arrayList of DrawerItems for the drawer
	     *
	     * @param profiles
	     * @return
	     */
	    public MSAccountHeaderBuilder withProfiles(ArrayList<IProfile> profiles) {
	    	getObject().withProfiles(profiles);
	        return this;
	    }
	    
	    public MSAccountHeaderBuilder withProfile(IProfile profile) {
	    	ArrayList<IProfile> profiles = new ArrayList();
	    	profiles.add(profile);
	    	getObject().withProfiles(profiles);
	        return this;
	    }

	    /**
	     * add single ore more DrawerItems to the Drawer
	     *
	     * @param profiles
	     * @return
	     */
	    public MSAccountHeaderBuilder addProfiles(IProfile... profiles) {
	    	getObject().addProfiles(profiles);
	        return this;
	    }
	    /**
	     * add a listener for the accountHeader
	     *
	     * @param onAccountHeaderListener
	     * @return
	     */
	    public MSAccountHeaderBuilder withOnAccountHeaderListener(OnAccountHeaderListener onAccountHeaderListener) {
	    	getObject().withOnAccountHeaderListener(onAccountHeaderListener);
	        return this;
	    }
	    /**
	     * @param drawer
	     * @return
	     */
	    public MSAccountHeaderBuilder withDrawer(Drawer.Result drawer) {
	    	getObject().withDrawer(drawer);
	        return this;
	    }
	    
	    public AccountHeader.Result build(){
	    	return getObject().build();
	    }
}
