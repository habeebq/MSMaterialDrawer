package com.maximussoft.msmaterialdrawer;


import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Adapter;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import anywheresoftware.b4a.AbsObjectWrapper;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.BA.DependsOn;
import anywheresoftware.b4a.objects.PanelWrapper;
import anywheresoftware.b4a.objects.ViewWrapper;

/* Changelog
 * Added SlideEvent
 * Added withDisplayBelowActionbar
 * Added 
 */


@BA.Author("thedesolatesoul")
@BA.ShortName("MSMaterialDrawerBuilder")
@BA.Version(0.93F)
@DependsOn(values = { "android-support-v4",
					  "android-support-v7-appcompat"	})
@BA.Events(values = {
		"Opened",
		"Closed",
		"Slide(SlideOffset as Float)",
		"ItemLongClick(Position as Int, Identifier as Int)",
		"ItemClick(Position as Int, Identifier as Int)"})

@BA.ActivityObject
public class MSMaterialDrawerBuilder extends AbsObjectWrapper<Drawer>   {
	private BA mBA;
	private String mEventName;
	private Drawer.Result result = null;
	
	public void Initialize(BA ba, String eventName){
		mBA = ba;
	    mEventName = eventName;

	   setObject(new Drawer().withActivity(ba.activity));
	   getObject().withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
		
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id, IDrawerItem drawerItem) {
			if(drawerItem != null){
				mBA.raiseEvent2(this, false, (mEventName + "_ItemClick").toLowerCase(mBA.cul),false, new Object[]{position, drawerItem.getIdentifier()});
			} else { //Header or footer
				View retV = null;
				try {
					ViewGroup v = (ViewGroup)view;
					retV = v.getChildAt(0);
				} catch(Exception e) {
					retV = null;
				} 
				mBA.raiseEvent2(retV, false, (mEventName + "_ItemClick").toLowerCase(mBA.cul),false, new Object[]{position, -1});
			}
		}
	   });
	   
	   getObject().withOnDrawerItemLongClickListener(new Drawer.OnDrawerItemLongClickListener() {
		
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view,
				int position, long id, IDrawerItem drawerItem) {
			if(drawerItem != null){
				mBA.raiseEvent2(this, false, (mEventName + "_ItemLongClick").toLowerCase(mBA.cul),false, new Object[]{position, drawerItem.getIdentifier()});
			} else { //Header or footer
				View retV = null;
				try {
					ViewGroup v = (ViewGroup)view;
					retV = v.getChildAt(0);
				} catch(Exception e) {
					retV = null;
				} 
				mBA.raiseEvent2(retV, false, (mEventName + "_ItemLongClick").toLowerCase(mBA.cul),false, new Object[]{position, -1});
			}
			return false;
		}
	   });
	   
	   getObject().withOnDrawerListener(new Drawer.OnDrawerListener() {
		
		@Override
		public void onDrawerOpened(View drawerView) {
			mBA.raiseEvent2(this, false, (mEventName + "_Opened").toLowerCase(mBA.cul),false, new Object[]{null});
		}
		
		@Override
		public void onDrawerClosed(View drawerView) {
			mBA.raiseEvent2(this, false, (mEventName + "_Closed").toLowerCase(mBA.cul),false, new Object[]{null});
		}

		@Override
		public void onDrawerSlide(View drawerView, float slideOffset) {
			mBA.raiseEvent2(this, false, (mEventName + "_Slide").toLowerCase(mBA.cul),false, new Object[]{slideOffset});
		}
	   });
	   
	}
	
	
	
	public MSMaterialDrawer Build(){
		result = getObject().build();
		MSMaterialDrawer md = new MSMaterialDrawer();
		md.setObject(result);
		return md;
	}
	
	public MSMaterialDrawerBuilder withToolbar(Toolbar toolbar){
		getObject().withToolbar(toolbar);
		return this;
	}
	
	public MSMaterialDrawerBuilder withActionbarToggle(boolean toggle){
		getObject().withActionBarDrawerToggle(toggle);
		return this;
	}

	public MSMaterialDrawerBuilder withActionbarToggleAnimated(boolean actionBarDrawerToggleAnimated){
		getObject().withActionBarDrawerToggleAnimated(actionBarDrawerToggleAnimated);
		return this;
	}

	public MSMaterialDrawerBuilder withTransluscentStatusbar(boolean translucentStatusBar){
		getObject().withTranslucentStatusBar(translucentStatusBar) ;
		return this;
	}

	public MSMaterialDrawerBuilder withDisplayBelowToolbar(boolean displayBelowToolbar){
		getObject().withDisplayBelowToolbar(displayBelowToolbar);
		return this;
	}
	
	
	public MSMaterialDrawerBuilder AddPrimaryDrawerItem(String name, Drawable icon, Drawable selectedIcon, String badge, boolean enabled, int identifier, String description){
		 PrimaryDrawerItem p = new PrimaryDrawerItem();
		 p.setName(name);
		 if (description!=""){
			 p.setDescription(description);}
		 if (icon!= null) {
			 p.setIcon(icon);}
		 if (selectedIcon != null){
			 p.withSelectedIcon(selectedIcon);}
		 p.withBadge(badge);
		 p.setEnabled(enabled);
		 p.withIdentifier(identifier);
		 getObject().addDrawerItems(p);
		 return this;
	 }

	public MSMaterialDrawerBuilder AddSectionDrawerItem(String name, boolean divider){
		 SectionDrawerItem p = new SectionDrawerItem();
		 p.setName(name);
		 p.setDivider(divider);
		 getObject().addDrawerItems(p);
		 return this;
	 }

	public MSMaterialDrawerBuilder AddDividerItem(){
		 DividerDrawerItem p = new DividerDrawerItem();
		 getObject().addDrawerItems(p);
		 return this;
	 }

	public MSMaterialDrawerBuilder AddSecondaryDrawerItem(String name, Drawable icon, Drawable selectedIcon, String badge, boolean enabled, int identifier){
		 SecondaryDrawerItem p = new SecondaryDrawerItem();
		 p.setName(name);
		 if (icon!= null) {
			 p.setIcon(icon);}
		 if (selectedIcon != null){
			 p.withSelectedIcon(selectedIcon);}
		 p.withBadge(badge);
		 p.setEnabled(enabled);
		 getObject().addDrawerItems(p);
		 p.withIdentifier(identifier);
		 return this;
	 }

	
	public MSMaterialDrawerBuilder withSelectedItem(int Position){
		getObject().withSelectedItem(Position);
		return this;
	}

	public MSMaterialDrawerBuilder withHeader(View headerView, int width, int height){
		LayoutParams params = new LayoutParams(width,height);
		headerView.setLayoutParams(params);
		getObject().withHeader(headerView);
		return this;
	}
	
	public MSMaterialDrawerBuilder withHeaderDivider(boolean enable){
		getObject().withHeaderDivider(enable);
		return this;
	}
	
	public MSMaterialDrawerBuilder withHeaderClickable(boolean headerClickable){
		getObject().withHeaderClickable(headerClickable);
		return this;
	}
	
	
	public MSMaterialDrawerBuilder withFooter(View footerView, int width, int height){
		LayoutParams params = new LayoutParams(width,height);
		footerView.setLayoutParams(params);
		getObject().withFooter(footerView);
		return this;
	}
	
	public MSMaterialDrawerBuilder withCloseOnClick(boolean closeOnClick){
		getObject().withCloseOnClick(closeOnClick);
		return this;
	}
	
	public MSMaterialDrawerBuilder withDrawerWidthDp(int drawerWidthDp){
		getObject().withDrawerWidthDp(drawerWidthDp);
		return this;
	}
	
	/*
	 * Set the gravity for the drawer. START, LEFT | RIGHT, END
	 */
	public MSMaterialDrawerBuilder withDrawerGravity(int gravity){
		getObject().withDrawerGravity(gravity);
		return this;
	}

	public MSMaterialDrawerBuilder AddPrimaryDrawerItem(MSPrimaryDrawerItem PrimaryDrawerItem){
		getObject().addDrawerItems(PrimaryDrawerItem.getObject());
		return this;
	}
	
	public MSMaterialDrawerBuilder AddSecondaryDrawerItem(MSSecondaryDrawerItem SecondaryDrawerItem){
		getObject().addDrawerItems(SecondaryDrawerItem.getObject());
		return this;
	}
	
	public MSMaterialDrawerBuilder withAccountHeader(AccountHeader.Result accountHeader){
		getObject().withAccountHeader(accountHeader);
		return this;
	}
	
}
