package com.maximussoft.msmaterialdrawer;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import anywheresoftware.b4a.BA;

import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

@BA.ShortName("MSProfile")
public class MSProfile implements IProfile<MSProfile> {
	   private int identifier = -1;

	    private boolean selectable = true;
	    private boolean nameShown = false;

	    private Drawable icon;

	    private String name;
	    private String email;

	    private boolean enabled = true;
	    private Object tag;

	    
	@Override
	public MSProfile withName(String name) {
		setName(name);
		return this;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public MSProfile withEmail(String email) {
		setEmail(email);
		return this;
	}

	@Override
	public String getEmail() {
		return email;
	}

	@Override
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public MSProfile withIcon(Drawable icon) {
		this.icon = icon;
		return this;
	}

	@Override
	public Drawable getIcon() {
		return this.icon;
	}

	@Override
	public void setIcon(Drawable icon) {
		this.icon = icon;
	}

	@Override
	public MSProfile withSelectable(boolean selectable) {
		this.selectable = selectable;
		return this;
	}

	@Override
	public boolean isSelectable() {
		return selectable;
	}

	@Override
	public MSProfile setSelectable(boolean selectable) {
		this.selectable = selectable;
		return this;
	}

	 public MSProfile withIdentifier(int identifier) {
	     this.identifier = identifier;
	     return this;
	 }
	  
	@Override
	public int getIdentifier() {
		return this.identifier;
	}
	
    public void setIdentifier(int identifier) {
        this.identifier = identifier;
    }

	@Override
	public MSProfile withIcon(String url) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MSProfile withIcon(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri getIconUri() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setIcon(String url) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setIcon(Uri uri) {
		// TODO Auto-generated method stub
		
	}


}
