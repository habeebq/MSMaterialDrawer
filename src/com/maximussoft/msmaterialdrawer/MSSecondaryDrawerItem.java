package com.maximussoft.msmaterialdrawer;

import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;

import android.graphics.drawable.Drawable;
import anywheresoftware.b4a.BA;
import anywheresoftware.b4a.AbsObjectWrapper;

@BA.ShortName("MSSecondaryDrawerItem")

public class MSSecondaryDrawerItem extends AbsObjectWrapper<SecondaryDrawerItem> {

	public void Initialize(BA ba){
		SecondaryDrawerItem p = new SecondaryDrawerItem();
		this.setObject(p);
	}
	
	public String getType(){
		return this.getObject().getType();
	}

	public String getName(){
		return this.getObject().getName();
	}
	
	public void setName(String pName){
		getObject().setName(pName);
	}
	
	public Object getTag(){
		return getObject().getTag();
	}
	
	public void setTag(Object tag){
		getObject().setTag(tag);
	}
	
	public Drawable getIcon(){
		return getObject().getIcon();
	}
	
	public void setIcon(Drawable icon){
		getObject().setIcon(icon);
	}
	
	public boolean isEnabled(){
		return getObject().isEnabled();
	}
	
	public void setEnabled(boolean enabled){
		getObject().setEnabled(enabled);
	}
	
	public void setBadge(String badge){
		getObject().setBadge(badge);
	}

	public String getBadge(){
		return getObject().getBadge();
	}

	public void setIdentifier(int identifier){
		getObject().setIdentifier(identifier);
	}

	public int getIdentifier(){
		return getObject().getIdentifier();
	}

}
