package com.mikepenz.materialdrawer;

import anywheresoftware.b4a.BA;

public class R {
	public static final class dimen {
		public static int abc_action_bar_default_height_material = BA.applicationContext.getResources().getIdentifier("abc_action_bar_default_height_material", "dimen", BA.packageName);
		public static int tool_bar_top_padding = BA.applicationContext.getResources().getIdentifier("tool_bar_top_padding", "dimen", BA.packageName);
		public static int material_drawer_padding = BA.applicationContext.getResources().getIdentifier("material_drawer_padding", "dimen", BA.packageName);
		public static int material_drawer_margin = BA.applicationContext.getResources().getIdentifier("material_drawer_margin", "dimen", BA.packageName);
		public static int material_drawer_vertical_padding = BA.applicationContext.getResources().getIdentifier("material_drawer_vertical_padding", "dimen", BA.packageName);
		public static int material_drawer_account_header_height_compact = BA.applicationContext.getResources().getIdentifier("material_drawer_account_header_height_compact", "dimen", BA.packageName);
		public static int material_drawer_width = BA.applicationContext.getResources().getIdentifier("material_drawer_width", "dimen", BA.packageName);
	}
	public static final class layout {
		public static int material_drawer = BA.applicationContext.getResources().getIdentifier("material_drawer", "layout", BA.packageName);
		public static int material_drawer_slider = BA.applicationContext.getResources().getIdentifier("material_drawer_slider", "layout", BA.packageName);
		public static int material_drawer_item_header = BA.applicationContext.getResources().getIdentifier("material_drawer_item_header", "layout", BA.packageName);
		public static int material_drawer_item_footer = BA.applicationContext.getResources().getIdentifier("material_drawer_item_footer", "layout", BA.packageName);
		public static int material_drawer_compact_header = BA.applicationContext.getResources().getIdentifier("material_drawer_compact_header", "layout", BA.packageName);
		public static int material_drawer_header = BA.applicationContext.getResources().getIdentifier("material_drawer_header", "layout", BA.packageName);
		public static int material_drawer_item_divider = BA.applicationContext.getResources().getIdentifier("material_drawer_item_divider", "layout", BA.packageName);
		public static int material_drawer_item_primary = BA.applicationContext.getResources().getIdentifier("material_drawer_item_primary", "layout", BA.packageName);
		public static int material_drawer_item_profile = BA.applicationContext.getResources().getIdentifier("material_drawer_item_profile", "layout", BA.packageName);
		public static int material_drawer_item_profile_setting = BA.applicationContext.getResources().getIdentifier("material_drawer_item_profile_setting", "layout", BA.packageName);
		public static int material_drawer_item_secondary = BA.applicationContext.getResources().getIdentifier("material_drawer_item_secondary", "layout", BA.packageName);
		public static int material_drawer_item_section = BA.applicationContext.getResources().getIdentifier("material_drawer_item_section", "layout", BA.packageName);
		public static int material_drawer_item_switch = BA.applicationContext.getResources().getIdentifier("material_drawer_item_switch", "layout", BA.packageName);
		public static int material_drawer_item_toggle = BA.applicationContext.getResources().getIdentifier("material_drawer_item_toggle", "layout", BA.packageName);
	}
	public static final class attr {
		public static int colorPrimaryDark = BA.applicationContext.getResources().getIdentifier("colorPrimaryDark", "attr", BA.packageName);
		public static int material_drawer_background = BA.applicationContext.getResources().getIdentifier("material_drawer_background", "attr", BA.packageName);
		public static int material_drawer_divider = BA.applicationContext.getResources().getIdentifier("material_drawer_divider", "attr", BA.packageName);
		public static int actionBarSize = BA.applicationContext.getResources().getIdentifier("actionBarSize", "attr", BA.packageName);
		public static int material_drawer_selected = BA.applicationContext.getResources().getIdentifier("material_drawer_selected", "attr", BA.packageName);
		public static int material_drawer_header_selection_text = BA.applicationContext.getResources().getIdentifier("material_drawer_header_selection_text", "attr", BA.packageName);
		public static int material_drawer_selected_text = BA.applicationContext.getResources().getIdentifier("material_drawer_selected_text", "attr", BA.packageName);
		public static int material_drawer_primary_text = BA.applicationContext.getResources().getIdentifier("material_drawer_primary_text", "attr", BA.packageName);
		public static int material_drawer_hint_text = BA.applicationContext.getResources().getIdentifier("material_drawer_hint_text", "attr", BA.packageName);
		public static int material_drawer_primary_icon = BA.applicationContext.getResources().getIdentifier("material_drawer_primary_icon", "attr", BA.packageName);
		public static int material_drawer_secondary_text = BA.applicationContext.getResources().getIdentifier("material_drawer_secondary_text", "attr", BA.packageName);
		public static int selectableItemBackground = BA.applicationContext.getResources().getIdentifier("selectableItemBackground", "attr", BA.packageName);
	}
	public static final class color {
		public static int material_drawer_primary_dark = BA.applicationContext.getResources().getIdentifier("material_drawer_primary_dark", "color", BA.packageName);
		public static int material_drawer_background = BA.applicationContext.getResources().getIdentifier("material_drawer_background", "color", BA.packageName);
		public static int material_drawer_divider = BA.applicationContext.getResources().getIdentifier("material_drawer_divider", "color", BA.packageName);
		public static int material_drawer_selected = BA.applicationContext.getResources().getIdentifier("material_drawer_selected", "color", BA.packageName);
		public static int material_drawer_header_selection_text = BA.applicationContext.getResources().getIdentifier("material_drawer_header_selection_text", "color", BA.packageName);
		public static int material_drawer_selected_text = BA.applicationContext.getResources().getIdentifier("material_drawer_selected_text", "color", BA.packageName);
		public static int material_drawer_primary_text = BA.applicationContext.getResources().getIdentifier("material_drawer_primary_text", "color", BA.packageName);
		public static int material_drawer_hint_text = BA.applicationContext.getResources().getIdentifier("material_drawer_hint_text", "color", BA.packageName);
		public static int material_drawer_primary_icon = BA.applicationContext.getResources().getIdentifier("material_drawer_primary_icon", "color", BA.packageName);
		public static int material_drawer_secondary_text = BA.applicationContext.getResources().getIdentifier("material_drawer_secondary_text", "color", BA.packageName);
		public static int primary = BA.applicationContext.getResources().getIdentifier("primary", "color", BA.packageName);
	}
	public static final class styleable {
		public static int biv_maskDrawable = BA.applicationContext.getResources().getIdentifier("biv_maskDrawable", "attr", BA.packageName);
		public static int biv_selectorOnPress = BA.applicationContext.getResources().getIdentifier("biv_selectorOnPress", "attr", BA.packageName);
		public static int BezelImageView_biv_maskDrawable = 0;
		public static int BezelImageView_biv_selectorOnPress = 1;
		
        public static final int[] BezelImageView = {
        	biv_maskDrawable, biv_selectorOnPress
        };
        public static final int[] CircularImageView = {
            0x7f01000a, 0x7f01000b, 0x7f01000c, 0x7f01000d,
            0x7f01000e, 0x7f01000f, 0x7f010010, 0x7f010011,
            0x7f010012, 0x7f010013, 0x7f010014, 0x7f010015
        };
        
		public static int siv_insetForeground = BA.applicationContext.getResources().getIdentifier("siv_insetForeground", "attr", BA.packageName);
        public static final int[] ScrimInsetsView = {
        	siv_insetForeground
        };

//		public static int BezelImageView = BA.applicationContext.getResources().getIdentifier("BezelImageView", "styleable", BA.packageName);
		public static int CircularImageViewStyle_circularImageViewDefault = BA.applicationContext.getResources().getIdentifier("CircularImageViewStyle_circularImageViewDefault", "styleable", BA.packageName);
		//public static int CircularImageView = BA.applicationContext.getResources().getIdentifier("CircularImageView", "styleable", BA.packageName);
		public static int CircularImageView_civ_border = BA.applicationContext.getResources().getIdentifier("CircularImageView_civ_border", "styleable", BA.packageName);
		public static int CircularImageView_civ_selector = BA.applicationContext.getResources().getIdentifier("CircularImageView_civ_selector", "styleable", BA.packageName);
		public static int CircularImageView_civ_shadow = BA.applicationContext.getResources().getIdentifier("CircularImageView_civ_shadow", "styleable", BA.packageName);
		public static int CircularImageView_civ_borderWidth = BA.applicationContext.getResources().getIdentifier("CircularImageView_civ_borderWidth", "styleable", BA.packageName);
		public static int CircularImageView_civ_borderColor = BA.applicationContext.getResources().getIdentifier("CircularImageView_civ_borderColor", "styleable", BA.packageName);
		public static int CircularImageView_civ_selectorColor = BA.applicationContext.getResources().getIdentifier("CircularImageView_civ_selectorColor", "styleable", BA.packageName);
		public static int CircularImageView_civ_selectorStrokeWidth = BA.applicationContext.getResources().getIdentifier("CircularImageView_civ_selectorStrokeWidth", "styleable", BA.packageName);
		public static int CircularImageView_civ_selectorStrokeColor = BA.applicationContext.getResources().getIdentifier("CircularImageView_civ_selectorStrokeColor", "styleable", BA.packageName);
		public static int CircularImageView_civ_shadowRadius = BA.applicationContext.getResources().getIdentifier("CircularImageView_civ_shadowRadius", "styleable", BA.packageName);
		public static int CircularImageView_civ_shadowDx = BA.applicationContext.getResources().getIdentifier("CircularImageView_civ_shadowDx", "styleable", BA.packageName);
		public static int CircularImageView_civ_shadowDy = BA.applicationContext.getResources().getIdentifier("CircularImageView_civ_shadowDy", "styleable", BA.packageName);
		public static int CircularImageView_civ_shadowColor = BA.applicationContext.getResources().getIdentifier("CircularImageView_civ_shadowColor", "styleable", BA.packageName);
		//public static int ScrimInsetsView = BA.applicationContext.getResources().getIdentifier("ScrimInsetsView", "styleable", BA.packageName);
		public static int ScrimInsetsView_siv_insetForeground = BA.applicationContext.getResources().getIdentifier("ScrimInsetsView_siv_insetForeground", "styleable", BA.packageName);
	}
	public static final class id {
		public static int sticky_header = BA.applicationContext.getResources().getIdentifier("sticky_header", "id", BA.packageName);
		public static int sticky_footer = BA.applicationContext.getResources().getIdentifier("sticky_footer", "id", BA.packageName);
		public static int divider = BA.applicationContext.getResources().getIdentifier("divider", "id", BA.packageName);
		public static int content_layout = BA.applicationContext.getResources().getIdentifier("content_layout", "id", BA.packageName);
		public static int account_header_drawer = BA.applicationContext.getResources().getIdentifier("account_header_drawer", "id", BA.packageName);
		public static int account_header_drawer_background = BA.applicationContext.getResources().getIdentifier("account_header_drawer_background", "id", BA.packageName);
		public static int account_header_drawer_text_section = BA.applicationContext.getResources().getIdentifier("account_header_drawer_text_section", "id", BA.packageName);
		public static int account_header_drawer_text_switcher = BA.applicationContext.getResources().getIdentifier("account_header_drawer_text_switcher", "id", BA.packageName);
		public static int account_header_drawer_current = BA.applicationContext.getResources().getIdentifier("account_header_drawer_current", "id", BA.packageName);
		public static int account_header_drawer_name = BA.applicationContext.getResources().getIdentifier("account_header_drawer_name", "id", BA.packageName);
		public static int account_header_drawer_email = BA.applicationContext.getResources().getIdentifier("account_header_drawer_email", "id", BA.packageName);
		public static int account_header_drawer_small_first = BA.applicationContext.getResources().getIdentifier("account_header_drawer_small_first", "id", BA.packageName);
		public static int account_header_drawer_small_second = BA.applicationContext.getResources().getIdentifier("account_header_drawer_small_second", "id", BA.packageName);
		public static int account_header_drawer_small_third = BA.applicationContext.getResources().getIdentifier("account_header_drawer_small_third", "id", BA.packageName);
		public static int icon = BA.applicationContext.getResources().getIdentifier("icon", "id", BA.packageName);
		public static int name = BA.applicationContext.getResources().getIdentifier("name", "id", BA.packageName);
		public static int description = BA.applicationContext.getResources().getIdentifier("description", "id", BA.packageName);
		public static int badge = BA.applicationContext.getResources().getIdentifier("badge", "id", BA.packageName);
		public static int profileIcon = BA.applicationContext.getResources().getIdentifier("profileIcon", "id", BA.packageName);
		public static int email = BA.applicationContext.getResources().getIdentifier("email", "id", BA.packageName);
		public static int switchView = BA.applicationContext.getResources().getIdentifier("switchView", "id", BA.packageName);
		public static int toggle = BA.applicationContext.getResources().getIdentifier("toggle", "id", BA.packageName);
	}
	public static final class string {
		public static int drawer_open = BA.applicationContext.getResources().getIdentifier("drawer_open", "string", BA.packageName);
		public static int drawer_close = BA.applicationContext.getResources().getIdentifier("drawer_close", "string", BA.packageName);
	}
}
