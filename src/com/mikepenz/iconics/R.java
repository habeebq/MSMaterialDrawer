package com.mikepenz.iconics;

import anywheresoftware.b4a.BA;

public class R {
	
	public static final class styleable {
		public static int iiv_icon = BA.applicationContext.getResources().getIdentifier("iiv_icon", "attr", BA.packageName);
		public static int iiv_color = BA.applicationContext.getResources().getIdentifier("iiv_color", "attr", BA.packageName);
		
        public static final int[] IconicsImageView = {
        	iiv_icon, iiv_color
        };
        public static int IconicsImageView_iiv_icon = BA.applicationContext.getResources().getIdentifier("IconicsImageView_iiv_icon", "styleable", BA.packageName);
        public static int IconicsImageView_iiv_color = BA.applicationContext.getResources().getIdentifier("IconicsImageView_iiv_color", "styleable", BA.packageName);
	}
}